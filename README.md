# 留守宠物

## 项目背景

因疫情防控需要，各地紧急出台最新严管令，大部分小区都实行封闭式管理，外地未返回人员劝阻，一下子让许多正准备返城的外地铲屎官原地蒙圈！自己留守在家里的毛孩子们可能快撑不下去了！

## 项目简介

留守宠物互助，上门喂养，宠物寄养。铲屎官们可以发布留守宠物求助信息获得帮助。项目基于腾讯地图实现根据地理位置查看附近求助信息，并集成了中国移动能力开放平台隐私号功能，有效保护求助者的隐私信息。

## 项目预览图片

![PERVIEW](https://images.gitee.com/uploads/images/2020/0219/005021_9c1bf0d8_21964.png)

![小程序码](https://images.gitee.com/uploads/images/2020/0208/004824_52736a2f_21964.png)

## 项目依赖

- [微信小程序云开发](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)
- [中国移动能力开放平台订单小号](https://ct.open.10086.cn/portal/)
- [腾讯位置服务地图选点](https://mp.weixin.qq.com/wxopen/pluginbasicprofile?action=intro&appid=wx76a9a06e5b4e693e&token=92939867&lang=zh_CN)


## 部署说明

- 下载本项目或者`git clone https://gitee.com/ghostmemory/tcb-hackthon-pet`;
- 微信小程序后台增加"地理位置选择"插件`wx76a9a06e5b4e693e`;
- 新建小程序项目选择云开发模式，并导入项目代码;
- 修改`/miniprogram/config/config.js`中的 CLOUDENV: '云开发环境ID'等相关参数;
- 创建云数据库合集`pet、user、share、axb、devlog`,其中`pet`合集中`geo`字段添加地理位置索引;
- 申请订单小号接口权限并修改`/cloudfunctions/cmcc/index.js`中的 APIKey等相关参数;


## 开发说明

- Fork 本仓库
- 新建 Feat_xxx 分支
- 提交代码
- 新建 Pull Request

## Bug 反馈

如果有 Bug ，请通过 [issues](https://gitee.com/ghostmemory/tcb-hackthon-pet/issues) 反馈


## 项目地址

[https://gitee.com/ghostmemory/tcb-hackthon-pet](https://gitee.com/ghostmemory/tcb-hackthon-pet)

[查看更多项目源码](https://developers.weixin.qq.com/community/servicemarket/share?view=template)


## LICENSE

[Apache-2.0](https://gitee.com/ghostmemory/tcb-hackthon-pet/blob/master/LICENSE)